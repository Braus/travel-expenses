import { Mongo } from 'meteor/mongo'

export const Expenses = new Mongo.Collection("expenses")

Expenses.allow({
    insert(){ return false },
    update(){ return false },
    remove(){ return false }
})

Expenses.deny({
    insert(){ return true },
    update(){ return true },
    remove(){ return true }
})

// const ExpenseSchema = new SimpleSchema({
//     date: {type: String},
//     country: {type: String},
//     city: {type: String},
//     category: {type: String},
//     type: {type: String},
//     cost: {type: Number},
//     notes: {type: String, optional: true},
// })
//
// Expenses.attachSchema(ExpenseSchema)
