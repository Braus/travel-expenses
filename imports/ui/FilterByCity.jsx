import React, { Component } from 'react'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

export default class FilterByCity extends Component {
    constructor(props) {
        super(props)

        this.state = {value: 0};
    }

    handleSearch(e){
        this.props.onFilter(e.target.value)
    }

    handleChange = (event, index, value) => {
        // console.log(event.target.innerHTML)
        this.props.onFilter(event.target.innerHTML)
        this.setState({value})
    }

    cityDropdown(){
        return this.props.cities.map((city, key) => (
            <MenuItem key={key + 1} value={key + 1} primaryText={city} />
        ))
    }

    render() {
        return(
            <DropDownMenu value={this.state.value} ref="currentCity" onChange={this.handleChange}>
                <MenuItem value={0} primaryText="City" />
                {this.cityDropdown()}
            </DropDownMenu>
        )
    }
}
