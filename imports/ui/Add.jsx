import React, {Component} from 'react';
import Avatar from 'material-ui/Avatar';
import {ListItem} from 'material-ui/List';
import ActionDeleteForever from 'material-ui/svg-icons/action/delete-forever'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker'


import { Expenses } from '../api/Expenses'

const styles = {
  floatRight:{
      float: 'right'
  }
}


export default class Add extends Component {

    constructor(props){
        super(props)

        this.state = {
            category: "Category",
            addVisible: false
        }
    }

    formatDate(date){
        return new Intl.DateTimeFormat(navigator.language).format(date)
    }

    submitExpense(event){
        event.preventDefault()
        const correctDateFormat = new Date(this.refs.date.state.date)


        let expense = {
            date: correctDateFormat.getDate() + '/' + (correctDateFormat.getMonth() + 1) + '/'  +  correctDateFormat.getFullYear(),
            country: this.refs.country.value,
            city: this.refs.city.value,
            category: this.refs.category.props.value,
            type: this.refs.type.value,
            cost: this.refs.cost.value,
            notes: this.refs.notes.value,
            createdAt:  new Date(),
        }

        Meteor.call("insertExpense", expense, (error) => {
            if(error){
                console.log("error trying to insert an expense", error)
            } else {
                document.getElementsByClassName('add-form')[0].reset()
            }
        })
    }

    handleCategoryChange = (event, index, value) => this.setState({category: event.target.innerHTML});
    toggleDisplay = () => this.setState({addVisible: !this.state.addVisible})

    render() {

        const displayAddBody = {
            display: this.state.addVisible ? 'block' : 'none'
        }

        return (
            <div className="row">
                <FloatingActionButton mini={true} style={styles.floatRight} onClick={this.toggleDisplay}> + </FloatingActionButton>
                <form className="col s12 add-form" onSubmit={this.submitExpense.bind(this)} style={displayAddBody}>
                    <div className="row">
                        <div className="input-field col s12 m3">
                            <DatePicker hintText="Date" ref="date" formatDate={this.formatDate.bind(this)} mode="landscape" autoOk={true} />
                        </div>
                        <div className="input-field col s12 m3">
                            <input placeholder="Country" ref="country" type="text" className="validate"></input>
                        </div>
                        <div className="input-field col s12 m3">
                            <input placeholder="City" ref="city" type="text" className="validate"></input>
                        </div>
                        <div className="input-field col s12 m3">
                            <DropDownMenu value={this.state.category} ref="category" onChange={this.handleCategoryChange}>
                                <MenuItem value="Category" primaryText="Category" />
                                <MenuItem value="Travel Cost" primaryText="Travel Cost" />
                                <MenuItem value="Food" primaryText="Food" />
                                <MenuItem value="Accommodation" primaryText="Accommodation" />
                                <MenuItem value="Miscelanous" primaryText="Miscelanous" />
                            </DropDownMenu>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s12 m3">
                            <input placeholder="Type" ref="type" type="text" className="validate"></input>
                        </div>
                        <div className="input-field col s12 m3">
                            <input placeholder="Cost" ref="cost" step="0.01" type="number" className="validate"></input>
                        </div>

                        <div className="input-field col s12 m3">
                            <input placeholder="Notes" ref="notes" type="text" className="validate"></input>
                        </div>
                        <div className="input-field col s12 m3">
                            <button type="submit" name="action" className="btn waves-effect waves-light light-blue darken-3">
                                Add
                                <i className="material-icons right">send</i>
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        )
    }
}
