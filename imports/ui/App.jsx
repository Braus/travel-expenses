// Core
import React, { Component } from 'react'

// Maerial UI
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import RaisedButton from 'material-ui/RaisedButton'
import AppBar from 'material-ui/AppBar'
import Divider from 'material-ui/Divider'
import { Table, TableRow, TableBody,TableHeader,TableRowColumn, TableHeaderColumn, TableFooter } from 'material-ui/Table'
import { createContainer } from 'meteor/react-meteor-data'

//Needed because the material ui event click is too fast
// therefore react does not detect it.
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()

// api
import { Expenses } from '../api/Expenses'

// Travel Expenses Components
import ExpensesList from './ExpensesList'
import FilterByCountry from './FilterByCountry'
import FilterByCity from './FilterByCity'
import Add from './Add'

export class App extends Component {
    constructor(props){
        super(props)

        this.state = {
            expenses: [],
            cities: [],
            countries:[]
        }

        this.searchCity = this.searchCity.bind(this)
        this.searchCountry = this.searchCountry.bind(this)

    }

    componentWillReceiveProps(nextProps){
        const cities = nextProps.expenses.map((expense) => (expense.city))
        const countries = nextProps.expenses.map((expense) => (expense.country))

        this.setState({
            cities: Array.from(new Set(cities)),
            countries: Array.from(new Set(countries))
        })

    }

    renderExpenses(){
        if (!this.state.expenses.length){
            return this.props.expenses.map((expense) => (
                <ExpensesList key={expense._id} expense={expense} />
            ))
        } else {
            return this.state.expenses.map((expense) => (
                <ExpensesList key={expense._id} expense={expense} />
            ))
        }
    }

    searchCity(q){
        const filtering = this.props.expenses.filter(function(el) {
            return el.city.toLowerCase() === q.toLowerCase()
        })

        this.setState({
            expenses: filtering
        })
    }

    searchCountry(q){
        const filtering = this.props.expenses.filter(function(el) {
            console.log(el.country)
            return el.country.toLowerCase() === q.toLowerCase()
        })

        this.setState({
            expenses: filtering
        })
    }


    add(a, b){
        return a + b
    }

    getTotalCost(){
        if (this.state.expenses.length){
            const getTotal = this.state.expenses.map( (expense) => (
                parseFloat(expense.cost)
            ))
            return getTotal.reduce(this.add, 0)

        }else{
            const getTotal = this.props.expenses.map( (expense) => (
                parseFloat(expense.cost)
            ))
            return getTotal.reduce(this.add, 0)
        }

    }

    render(){
        return(
            <MuiThemeProvider>
                <div className="container">
                    <AppBar
                        title="Travel Expenses"
                        iconClassNameRight="muidocs-icon-navigation-expand-more"
                        showMenuIconButton={false}>

                    </AppBar>

                    <div className="row">
                        <div className="col s12"><Add /></div>
                        <div className="col s12">
                            <span>Filters: </span>
                            <FilterByCountry
                                countries={this.state.countries}
                                onFilter={this.searchCountry}
                            />
                            <FilterByCity
                                cities={this.state.cities}
                                onFilter={this.searchCity}
                            />
                        </div>
                        <div className="col s12">
                            <Divider />
                            <Table>
                                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                    <TableRow>
                                        <TableHeaderColumn>Date</TableHeaderColumn>
                                        <TableHeaderColumn>Country</TableHeaderColumn>
                                        <TableHeaderColumn>City</TableHeaderColumn>
                                        <TableHeaderColumn>Category</TableHeaderColumn>
                                        <TableHeaderColumn>Type</TableHeaderColumn>
                                        <TableHeaderColumn>Cost</TableHeaderColumn>
                                    </TableRow>
                                </TableHeader>
                                <TableBody displayRowCheckbox={false}>
                                    {this.renderExpenses()}
                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TableRowColumn>
                                            Total:{this.getTotalCost()}
                                        </TableRowColumn>
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </div>

                    </div>

                </div>
            </MuiThemeProvider>
        )
    }
}

// TODO: Add propTypes
// App.propTypes = {
//     expenses: PropTypes.array.isRequired
// }

export default createContainer(() => {
    Meteor.subscribe('expenses')


    return {
        expenses: Expenses.find({}, {
            sort:{
                date: 1
            }
        }).fetch()
    }
}, App)
