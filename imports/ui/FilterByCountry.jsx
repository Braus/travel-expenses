import React, { Component } from 'react'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

export default class FilterByCountry extends Component {
    constructor(props) {
        super(props)

        this.state = {value: 0};
    }

    handleSearch(e){
        this.props.onFilter(e.target.value)
    }

    handleChange = (event, index, value) => {
        this.props.onFilter(event.target.innerHTML)
        this.setState({value})
    }

    countryDropdown(){
        return this.props.countries.map((country, key) => (
            <MenuItem key={key + 1} value={key + 1} primaryText={country} />
        ))
    }
    
    render() {
        return(
            <DropDownMenu value={this.state.value} onChange={this.handleChange}>
                <MenuItem value={0} primaryText="Country" />
                {this.countryDropdown()}
            </DropDownMenu>
        )
    }
}
