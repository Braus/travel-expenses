import React, { Component } from 'react'
import { TableRow, TableRowColumn } from 'material-ui/Table';


export default class ExpensesList extends Component {
    constructor(props){
        super(props)

    }

    render(){
        const expense = this.props.expense

        return (
            <TableRow selectable={false}>
                <TableRowColumn>{expense.date}</TableRowColumn>
                <TableRowColumn>{expense.country}</TableRowColumn>
                <TableRowColumn>{expense.city}</TableRowColumn>
                <TableRowColumn>{expense.category}</TableRowColumn>
                <TableRowColumn>{expense.type}</TableRowColumn>
                <TableRowColumn>{expense.cost}</TableRowColumn>
            </TableRow>
        )
    }
}
