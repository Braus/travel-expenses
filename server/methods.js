import { Meteor } from 'meteor/meteor'
import { Expenses } from '../imports/api/Expenses'

Meteor.methods({
    insertExpense(expense){
         Expenses.insert(expense)
    },
    updateExpense(expense){
        Expenses.update(expense._id, { $set: expense })
    },
    deleteExpense(expenseId){
        Expenses.remove(expenseId)
    }
})
