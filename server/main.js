import { Meteor } from 'meteor/meteor';
import { Expenses } from '../imports/api/Expenses'

Meteor.startup(() => {
  // code to run on server at startup
  if(Meteor.isServer){
       Meteor.publish('expenses', function() {
           return Expenses.find({});
       })
   }
});
